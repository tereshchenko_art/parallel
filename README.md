# TPS Parallel studio
## How to run
Make sure your have Node.js installed (https://nodejs.org/uk/)


- open terminal in project dir
- clone project [git clone https://tereshchenko_art@bitbucket.org/tereshchenko_art/parallel.git] 
- intall dependencies [npm i]
- run server [npm start]
- open localhost:3000 in your browser
