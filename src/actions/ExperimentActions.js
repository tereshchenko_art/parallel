import {
  SET_TYPOGRAPHY,
  SET_EXPERIMENT_STATE,
  SET_EXPERIMENT_PROGRESS,
  RESET_EXPERIMENT_STATE,
  SET_TIME_SPENT,
  SET_PROCESSOR_SPEED,
  SET_LATENCY,
  SET_NETWORK_SPEED,
  SET_PROBLEM,
  STOP_EXPERIMENT,
  SET_PROCESSOR_NUMBER
} from '../constants/ActionTypes';

import { CIRCLE, GRID, LINE } from '../constants/Topography';

export const setTypography = typography => dispatch => {
  dispatch(
    {
      type: SET_TYPOGRAPHY,
      typography
    }
  );

  switch (typography) {
    case CIRCLE:
      dispatch(
        {
          type: SET_PROCESSOR_NUMBER,
          processorNumber: 0
        }
      );
      break;

    case LINE:
      dispatch(
        {
          type: SET_PROCESSOR_NUMBER,
          processorNumber: 6
        }
      );
      break;

    case GRID:
      dispatch(
        {
          type: SET_PROCESSOR_NUMBER,
          processorNumber: 4
        }
      );
      break;
  }

};

export const setTimeSpent = timeSpent => ({
  type: SET_TIME_SPENT,
  timeSpent
});

export const setExperimentState = state => {
  return {
    type: SET_EXPERIMENT_STATE,
    state
  }
};

export const setExperimentProgress = progress => ({
  type: SET_EXPERIMENT_PROGRESS,
  progress
});

export const resetExperimentState = () => ({
  type: RESET_EXPERIMENT_STATE
});

export const setProcessorSpeed = processorSpeed => ({
  type: SET_PROCESSOR_SPEED,
  processorSpeed
});

export const setLatency = latency => ({
  type: SET_LATENCY,
  latency
});

export const setNetworkSpeed = networkSpeed => ({
  type: SET_NETWORK_SPEED,
  networkSpeed
});

export const setProblem = problem => ({
  type: SET_PROBLEM,
  problem
});

export const stopExperiment = () => ({
  type: STOP_EXPERIMENT
});

export const setProcessorNumber = processorNumber => ({
  type: SET_PROCESSOR_NUMBER,
  processorNumber
});