import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { Router, Route, IndexRoute, hashHistory } from 'react-router'

import App from './containers/App'
import './styles/reset.scss'
import configureStore from './store/configureStore'
import WelcomePage from './components/WelcomePage';
import ExperimentContainer from './containers/ExperimentContainer';
import StatisticsContainer from './containers/StatisticsContainer';
import LinearArch from './containers/LinearArch';
import GridArch from './containers/GridArch';
import CircleArch from './containers/CircleArch';

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
//injectTapEventPlugin();

const store = configureStore();

render(
  <Provider store={store}>
    <Router history={hashHistory}>
      <Route path='/' component={App}>
        <IndexRoute component={WelcomePage} />
        <Route path='experiment' component={ExperimentContainer} >
          <Route path='linear' component={LinearArch} />
          <Route path='grid' component={GridArch} />
          <Route path='circle' component={CircleArch} />
        </Route>
        <Route path='statistics' component={StatisticsContainer} />
      </Route>
    </Router>
  </Provider>,
  document.getElementById('root')
);


