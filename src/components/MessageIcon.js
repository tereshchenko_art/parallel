import React, { PropTypes } from 'react';
import MdMail from 'react-icons/lib/md/mail'

import { MESSAGE_ICON_SIZE } from '../constants/IconSizes';

import './Icon.scss';

const MessageIcon = ({ style, onTransitionEnd, rotationDeg }) => {
  return <div className={'icon'} style={{...style, transform: `rotate(${rotationDeg}deg)` }} onTransitionEnd={onTransitionEnd}>
    <MdMail size={MESSAGE_ICON_SIZE}/>
  </div>;
};

MessageIcon.propTypes = {
  style: PropTypes.shape({
    top: PropTypes.string.isRequired,
    left: PropTypes.string.isRequired
  }),
  onTransitionEnd: PropTypes.func.isRequired,
  rotationDeg: PropTypes.number
};

MessageIcon.defaultProps = {
  transform: 0
};

export default MessageIcon;