import React, { PropTypes } from 'react';
import MdDesktopWindows from 'react-icons/lib/md/desktop-windows'
import cn from 'classnames';

import { COMPUTER_ICON_SIZE } from '../constants/IconSizes';

import './Icon.scss';

const ComputerIcon = ({ style, className, rotationDeg }) => {
  return <div className={cn(className, 'icon')} style={{ ...style, transform: `rotate(${rotationDeg}deg)` }}>
    <MdDesktopWindows size={COMPUTER_ICON_SIZE}/></div>;
};

ComputerIcon.propTypes = {
  className: PropTypes.string,
  style: PropTypes.shape({
    top: PropTypes.string.isRequired,
    left: PropTypes.string.isRequired
  }),
  rotationDeg: PropTypes.number
};

ComputerIcon.defaultProps = {
  className: undefined,
  rotationDeg: 0
};

export default ComputerIcon;