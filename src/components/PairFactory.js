import React from 'react';
import PropTypes from 'prop-types';

import AnimationPair from './AnimationPair';

const PairFactory = ({ quantity }) => {
  return <div>{new Array(quantity).map((animationPair, index) => <AnimationPair key={index}/>)}</div>
};

PairFactory.propTypes = {
  quantity: PropTypes.number.isRequired
};

export default PairFactory;