import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import ComputerIcon from './ComputerIcon';
import MessageIcon from './MessageIcon';
import { TOP, LEFT } from './ComponentPositions';
import { generateTransition } from '../utils/utils';

import './AnimationPair.scss';

const INITIAL_LEFT_POSITION = 0;
const COMPUTER_ICON_SIZE = 50;
const INITIAL_TOP_POSITION = 0;
const DESTINATION_LEFT_POSITION = 150;
const NEIGHBOUR_OFFSET = DESTINATION_LEFT_POSITION - COMPUTER_ICON_SIZE;
const INITIAL_MESSAGE_TOP_POSITION = 10;
const INITIAL_MESSAGE_LEFT_POSITION = INITIAL_LEFT_POSITION;
const DESTINATION_MESSAGE_LEFT_POSITION = DESTINATION_LEFT_POSITION;

const INITIAL_LEFT_MESSAGE_STATE = {
  leftMessagePosition: [INITIAL_MESSAGE_TOP_POSITION, INITIAL_MESSAGE_LEFT_POSITION],
  isLeftMessageVisible: false,
  isLeftTransitionEnabled: false
};

const INITIAL_RIGHT_MESSAGE_STATE = {
  rightMessagePosition: [INITIAL_MESSAGE_TOP_POSITION, DESTINATION_MESSAGE_LEFT_POSITION],
  isRightMessageVisible: false,
  isRightTransitionEnabled: false
};


export default class AnimationPair extends Component {
  constructor() {
    super();

    this.firstComputerPosition = [INITIAL_TOP_POSITION, INITIAL_LEFT_POSITION];
    this.secondComputerPosition = [INITIAL_TOP_POSITION, DESTINATION_LEFT_POSITION];
    this.state = {
      ...INITIAL_LEFT_MESSAGE_STATE,
      ...INITIAL_RIGHT_MESSAGE_STATE
    }
  }

  static get propTypes() {
    return {
      className: PropTypes.string,
      animationDuration: PropTypes.number,
      isLeftComputerInvisible: PropTypes.bool,
      isRightComputerInvisible: PropTypes.bool,
      rotationDeg: PropTypes.number
    };
  }

  static get defaultProps() {
    return {
      className: undefined,
      animationDuration: 2,
      isLeftComputerInvisible: false,
      isRightComputerInvisible: false,
      rotationDeg: 0
    }
  }

  initLeftMessageAnimation() {
    this.setState({
      isLeftMessageVisible: true,
      isLeftTransitionEnabled: true
    });
  }

  initRightMessageAnimation() {
    this.setState({
      isRightMessageVisible: true,
      isRightTransitionEnabled: true
    });
  }

  animateFromLeftToRight = () => {
    this.initLeftMessageAnimation();

    this.setState({
      leftMessagePosition: [INITIAL_MESSAGE_TOP_POSITION, DESTINATION_MESSAGE_LEFT_POSITION]
    });
  };

  animateToBottomRight() {
    this.initRightMessageAnimation();

    this.setState({
      rightMessagePosition: [DESTINATION_LEFT_POSITION, DESTINATION_LEFT_POSITION]
    });
  }

  animateToTopRight() {
    this.initRightMessageAnimation();

    this.setState({
      rightMessagePosition: [-DESTINATION_LEFT_POSITION, DESTINATION_LEFT_POSITION]
    });
  }

  animateToTopLeft() {
    this.initLeftMessageAnimation();

    this.setState({
      leftMessagePosition: [-DESTINATION_LEFT_POSITION, INITIAL_LEFT_POSITION]
    })
  }


  animateToBottomLeft() {
    this.initLeftMessageAnimation();

    this.setState({
      leftMessagePosition: [DESTINATION_LEFT_POSITION, INITIAL_LEFT_POSITION]
    })
  }

  animateFromRightToLeft = () => {
    this.initRightMessageAnimation();

    this.setState({
      rightMessagePosition: [INITIAL_MESSAGE_TOP_POSITION, INITIAL_MESSAGE_LEFT_POSITION]
    });
  };

  animateToLeftNeighbour = () => {
    this.initLeftMessageAnimation();

    this.setState({
      leftMessagePosition: [INITIAL_MESSAGE_TOP_POSITION, -DESTINATION_MESSAGE_LEFT_POSITION]
    });
  };

  animateToRightNeighbour = () => {
    this.initRightMessageAnimation();

    this.setState({
      rightMessagePosition: [INITIAL_MESSAGE_TOP_POSITION, DESTINATION_MESSAGE_LEFT_POSITION * 2]
    });
  };

  animateBoth = () => {
    this.animateFromLeftToRight();
    this.animateFromRightToLeft();
  };

  animateToTopBoth() {
    this.animateToTopLeft();
    this.animateToTopRight();
  }

  animateToBottomBoth() {
    this.animateToBottomLeft();
    this.animateToBottomRight();
  }

  handleLeftMessageTransitionEnd = () => {
    this.setState({ ...INITIAL_LEFT_MESSAGE_STATE });
  };

  handleRightMessageTransitionEnd = () => {
    this.setState({ ...INITIAL_RIGHT_MESSAGE_STATE });
  };

  render() {
    const {
      firstComputerPosition,
      secondComputerPosition,
      handleLeftMessageTransitionEnd,
      handleRightMessageTransitionEnd
    } = this;

    const {
      leftMessagePosition,
      isLeftMessageVisible,
      rightMessagePosition,
      isRightMessageVisible,
      isRightTransitionEnabled,
      isLeftTransitionEnabled
    } = this.state;

    const {
      className,
      animationDuration,
      isLeftComputerInvisible,
      isRightComputerInvisible,
      rotationDeg
    } = this.props;

    return <div style={{ transform: `rotate(${rotationDeg}deg)` }}
                className={cn(className, 'animation-pair-container')}>
      <ComputerIcon rotationDeg={-rotationDeg} className={cn({ 'invisible': isLeftComputerInvisible })}/>
      <MessageIcon
        rotationDeg={-rotationDeg}
        onTransitionEnd={handleLeftMessageTransitionEnd} style={{
        left: leftMessagePosition[LEFT],
        top: leftMessagePosition[TOP],
        opacity: isLeftMessageVisible ? 1 : 0,
        transition: isLeftTransitionEnabled ? generateTransition(animationDuration) : 'none'
      }}/>

      <MessageIcon
        rotationDeg={-rotationDeg} onTransitionEnd={handleRightMessageTransitionEnd} style={{
        left: rightMessagePosition[LEFT],
        top: rightMessagePosition[TOP],
        opacity: isRightMessageVisible ? 1 : 0,
        transition: isRightTransitionEnabled ? generateTransition(animationDuration) : 'none'
      }}/>

      <ComputerIcon rotationDeg={-rotationDeg} className={cn({ 'invisible': isRightComputerInvisible })} style={{
        top: `${firstComputerPosition[TOP]}`,
        left: `${secondComputerPosition[LEFT]}`
      }}/>
    </div>
  }
}
