import { Map } from 'immutable';

import {
  SET_EXPERIMENT_STATE,
  SET_EXPERIMENT_PROGRESS,
  RESET_EXPERIMENT_STATE,
  SET_TIME_SPENT,
  SET_PROCESSOR_SPEED,
  SET_LATENCY,
  SET_NETWORK_SPEED,
  SET_PROBLEM,
  STOP_EXPERIMENT,
  SET_PROCESSOR_NUMBER
} from '../constants/ActionTypes';
import { MATRIX_BY_VECTOR } from '../constants/Problems';
import { LINE } from '../constants/Topography';
import { IDLE } from '../constants/ExperimentStates';
import { SET_TYPOGRAPHY } from '../constants/ActionTypes';

const PROGRESS_INITIAL_STATE = 0;
const TIME_SPENT_INITIAL_STATE = 0;

const initialState = Map({
  experimentState: IDLE,
  experimentProgress: PROGRESS_INITIAL_STATE,
  timeSpent: TIME_SPENT_INITIAL_STATE,
  processorSpeed: 0.5,
  latency: 0,
  networkSpeed: 10,
  problem: MATRIX_BY_VECTOR,
  typography: LINE,
  processorsNumber: 6
});

const experiment = (state = initialState, action) => {
  switch (action.type) {
    case SET_EXPERIMENT_STATE:
      return state.set('experimentState', action.state);

    case SET_TYPOGRAPHY:
      return state.set('typography', action.typography);

    case SET_EXPERIMENT_PROGRESS:
      return state.set('experimentProgress', action.progress);

    case SET_TIME_SPENT:
      return state.set('timeSpent', action.timeSpent);

    case SET_PROCESSOR_SPEED:
      return state.set('processorSpeed', action.processorSpeed);

    case SET_LATENCY:
      return state.set('latency', action.latency);

    case SET_NETWORK_SPEED:
      return state.set('networkSpeed', action.networkSpeed);

    case SET_PROBLEM:
      return state.set('problem', action.problem);

    case STOP_EXPERIMENT:
      return state.merge({
        experimentProgress: PROGRESS_INITIAL_STATE,
        timeSpent: TIME_SPENT_INITIAL_STATE
      });

    case SET_PROCESSOR_NUMBER:
      return state.set('processorsNumber', action.processorNumber);

    case RESET_EXPERIMENT_STATE:
      return initialState;

    default:
      return state
  }
};

export default experiment