import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux';

import ui from './ui';
import experiment from './experiment';

export default combineReducers({
  ui,
  experiment,
  routing: routerReducer
});

