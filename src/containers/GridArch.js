import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import AnimationPair from '../components/AnimationPair';
import { RUNNING, IDLE } from '../constants/ExperimentStates';
import { setExperimentProgress, setTimeSpent, stopExperiment } from '../actions/ExperimentActions';
import { ANIMATION_DURATION } from '../constants/Animation';

const ANIMATION_STEP_PROGRESS = 100 / 4;

import './GridArch.scss';

class GridArch extends Component {
  constructor(props) {
    super(props);

    this.pair = [];
    this.callbacs = [];

    this.resolveFunctions()
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.experimentState === IDLE && nextProps.experimentState === RUNNING) {
      this.startFunction();
    }

    if (this.props.experimentState === RUNNING && nextProps.experimentState === IDLE) {
      this.stop();
    }

    if (this.props.processorsNumber !== nextProps.processorsNumber) {
      this.resolveFunctions(nextProps.processorsNumber);
    }
  }

  resolveFunctions(number) {
    const procNumber = number ? number : this.props.processorsNumber;

    switch (procNumber) {
      case 9:
        this.renderFunction = this.renderFunc_9;
        this.startFunction = this.start_9;
        this.animationStepProgress = 100 / 8;
        break;

      case 4:
        this.renderFunction = this.renderFunc_4;
        this.startFunction = this.start_4;
        this.animationStepProgress = 100 / 4;
        break;

    }
  }

  stop() {
    this.pair.forEach(pair => {
      if (pair === null) {
        return;
      }

      pair.handleLeftMessageTransitionEnd();
      pair.handleRightMessageTransitionEnd();
    });

    this.callbacs.forEach(timeoutID => {
      clearTimeout(timeoutID);
    });

    this.props.stopExperiment();
  }

  animateBoth() {
    this.pair[0].animateBoth();
    this.pair[1].animateBoth();
  }

  phase_1_9() {
    this.pair[0].animateBoth();
    this.pair[1].animateBoth();
    this.pair[2].animateBoth();
    this.pair[3].animateToBottomRight();
    this.pair[4].animateToTopRight();
  }

  phase_2_9() {
    this.pair[3].animateBoth();
    this.pair[4].animateBoth();
    this.pair[5].animateBoth();
    this.pair[1].animateToBottomLeft();
    this.pair[2].animateToTopLeft();
  }

  start_9() {
    const { setExperimentProgress, setTimeSpent } = this.props;

    this.phase_1_9();
    setExperimentProgress(this.props.experimentProgress + ANIMATION_STEP_PROGRESS);
    this.callbacs.push(setTimeout(() => {
      this.phase_2_9();
      setExperimentProgress(this.props.experimentProgress + ANIMATION_STEP_PROGRESS);
      this.callbacs.push(setTimeout(() => {
        this.phase_1_9();
        //setTimeSpent(38);
        setExperimentProgress(this.props.experimentProgress + ANIMATION_STEP_PROGRESS);
        this.callbacs.push(setTimeout(() => {
          //setTimeSpent(60);
          this.phase_2_9();
          this.callbacs.push(
            setTimeout(() => {
              //setTimeSpent(60);
              this.phase_1_9();
              this.callbacs.push(setTimeout(() => {
                //setTimeSpent(60);
                this.phase_2_9();
                this.callbacs.push(
                  setTimeout(() => {
                    //setTimeSpent(60);
                    this.phase_1_9();
                    this.callbacs.push(
                      setTimeout(() => {
                        //setTimeSpent(60);
                        this.phase_2_9();
                        this.callbacs.push(setTimeout(() => {
                          setExperimentProgress(this.props.experimentProgress + ANIMATION_STEP_PROGRESS);
                        }, ANIMATION_DURATION));
                        setExperimentProgress(this.props.experimentProgress + ANIMATION_STEP_PROGRESS);
                      }, ANIMATION_DURATION)
                    );
                    setExperimentProgress(this.props.experimentProgress + ANIMATION_STEP_PROGRESS);
                  }, ANIMATION_DURATION)
                );
                setExperimentProgress(this.props.experimentProgress + ANIMATION_STEP_PROGRESS);
              }, ANIMATION_DURATION));
              setExperimentProgress(this.props.experimentProgress + ANIMATION_STEP_PROGRESS);
            }, ANIMATION_DURATION)
          );
          setExperimentProgress(this.props.experimentProgress + ANIMATION_STEP_PROGRESS);
        }, ANIMATION_DURATION))
      }, ANIMATION_DURATION));
    }, ANIMATION_DURATION));

  }

  start_4() {
    const { setExperimentProgress, setTimeSpent } = this.props;

    this.animateBoth();
    setExperimentProgress(this.props.experimentProgress + ANIMATION_STEP_PROGRESS);
    this.callbacs.push(setTimeout(() => {
      this.pair[0].animateToBottomRight();
      this.pair[1].animateToTopRight();
      setTimeSpent(20);
      setExperimentProgress(this.props.experimentProgress + ANIMATION_STEP_PROGRESS);
      this.callbacs.push(setTimeout(() => {
        this.animateBoth();
        setTimeSpent(38);
        setExperimentProgress(this.props.experimentProgress + ANIMATION_STEP_PROGRESS);
        this.callbacs.push(setTimeout(() => {
          setTimeSpent(60);
          setExperimentProgress(this.props.experimentProgress + ANIMATION_STEP_PROGRESS);
        }, ANIMATION_DURATION))
      }, ANIMATION_DURATION));
    }, ANIMATION_DURATION));

  }

  renderFunc_4() {
    return <div>
      <AnimationPair ref={pair => this.pair[0] = pair}/>
      <AnimationPair ref={pair => this.pair[1] = pair} className={'secondPair'}/>
    </div>;
  }

  renderFunc_9() {
    return <div>
      <AnimationPair ref={pair => this.pair[0] = pair}/>
      <AnimationPair ref={pair => this.pair[1] = pair} className={'secondPair'}/>
      <AnimationPair ref={pair => this.pair[2] = pair} className={'secondPair'}/>
      <AnimationPair ref={pair => this.pair[3] = pair} className={'fourth-pair_6'}/>
      <AnimationPair ref={pair => this.pair[4] = pair} className={'fifth-pair_6'}/>
      <AnimationPair ref={pair => this.pair[5] = pair} className={'sixth-pair_6'}/>
    </div>
  }

  render() {
    return <div style={{ width: '201px' }} className='grid-arch-container'>
      {this.renderFunction}
    </div>
  }
}

const select = state => {
  const { experiment } = state;

  return {
    experimentState: experiment.get('experimentState'),
    experimentProgress: experiment.get('experimentProgress')
  }
};

export default connect(select, dispatch =>
  bindActionCreators(
    { setExperimentProgress, setTimeSpent, stopExperiment },
    dispatch
  )
)(GridArch);