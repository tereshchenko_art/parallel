import * as React from 'react';
import { Line } from 'rc-progress';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'

import './Progress.scss';

const Progress = ({ experimentProgress }) => {
  return <div className={'progress-container'}><Line percent={experimentProgress} strokeColor={'rgb(0, 188, 212)'}
                                                     strokeWidth={'2'}/>
  </div>
};

Progress.propTypes = {
  experimentProgress: PropTypes.number.isRequired
};

const select = state => {
  const { experiment } = state;

  return {
    experimentProgress: experiment.get('experimentProgress')
  }
};

export default connect(select)(Progress);