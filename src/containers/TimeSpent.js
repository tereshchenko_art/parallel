import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import cn from 'classnames';

import './TimeSpent.scss';

class TimeSpent extends Component {
  static get propTypes() {
    return {
      className: PropTypes.string
    }
  }

  static get defaultProps() {
    return {
      className: undefined
    }
  }

  render() {
    return <time className={cn('timeContainer', this.props.className)}>{`Общие затраты времени: ${this.props.timeSpent} мкс`}</time>
  }
}

const select = state => ({
  timeSpent: state.experiment.get('timeSpent')
});

export default connect(select)(TimeSpent);