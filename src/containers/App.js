import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import '../styles/reset.scss';
import './App.scss';

const App = ({children}) => (
  <MuiThemeProvider>
    <div>
      {children}
    </div>

  </MuiThemeProvider>
);

export default App;
