import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import AnimationPair from '../components/AnimationPair';
import { RUNNING, IDLE } from '../constants/ExperimentStates';
import { setExperimentProgress, setTimeSpent, stopExperiment } from '../actions/ExperimentActions';
import { ANIMATION_DURATION } from '../constants/Animation';

const ANIMATION_STEP_PROGRESS = 100 / 5;

import './CircleArch.scss';

class StarArch extends Component {
  constructor() {
    super();

    this.pair = [];
    this.callbacs = [];
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.experimentState === IDLE && nextProps.experimentState === RUNNING) {
      this.start();
    }

    if (this.props.experimentState === RUNNING && nextProps.experimentState === IDLE) {
      this.stop();
    }
  }

  firstPhase = () => {
    this.pair[1].animateBoth();
    this.pair[4].animateBoth();
  };

  secondPhase = () => {
    this.pair[0].animateBoth();
    this.pair[2].animateBoth();
  };

  stop() {
    this.pair.forEach(pair => {
      pair.handleLeftMessageTransitionEnd();
      pair.handleRightMessageTransitionEnd();
    });

    this.callbacs.forEach(timeoutID => {
      clearTimeout(timeoutID);
    });

    this.props.stopExperiment();
  }

  start() {
    const { setExperimentProgress, setTimeSpent } = this.props;

    this.firstPhase();
    this.callbacs.push(setTimeout(() => {
      setTimeSpent(20);
      setExperimentProgress(this.props.experimentProgress + ANIMATION_STEP_PROGRESS);
      this.secondPhase();
      this.callbacs.push(setTimeout(() => {
        setTimeSpent(38);
        setExperimentProgress(this.props.experimentProgress + ANIMATION_STEP_PROGRESS);
        this.firstPhase();
        this.callbacs.push( setTimeout(() => {
          setTimeSpent(60);
          setExperimentProgress(this.props.experimentProgress + ANIMATION_STEP_PROGRESS);
          this.secondPhase();
          this.callbacs.push(setTimeout(() => {
            setTimeSpent(80);
            setExperimentProgress(this.props.experimentProgress + ANIMATION_STEP_PROGRESS);
            this.firstPhase();
            this.callbacs.push( setTimeout(() => {
              setTimeSpent(120);
              setExperimentProgress(this.props.experimentProgress + ANIMATION_STEP_PROGRESS);
            }, ANIMATION_DURATION))
          }, ANIMATION_DURATION))
        }, ANIMATION_DURATION))
      }, ANIMATION_DURATION))
    }, ANIMATION_DURATION))
  }
  
  render() {
    return <div className={'start-arch-container'}>
      <AnimationPair ref={pair => this.pair[0] = pair} rotationDeg={-50} isLeftComputerInvisible={true}
                     className={'first-pair_star'}/>
      <AnimationPair ref={pair => this.pair[1] = pair} rotationDeg={-90} className={'second-pair_star'}/>
      <AnimationPair ref={pair => this.pair[2] = pair} className={'third-pair_start'}/>
      <AnimationPair ref={pair => this.pair[3] = pair} isLeftComputerInvisible={true} rotationDeg={42}
                     className={'fourth-pair_start'}/>
      <AnimationPair ref={pair => this.pair[4] = pair} isLeftComputerInvisible={true} isRightComputerInvisible={true}
                     className={'fifth-pair_star'} rotationDeg={288}/>
    </div>
  }
}

const select = state => {
  const { experiment } = state;

  return {
    experimentState: experiment.get('experimentState'),
    experimentProgress: experiment.get('experimentProgress')
  }
};

export default connect(select, dispatch =>
  bindActionCreators(
    { setExperimentProgress, setTimeSpent, stopExperiment },
    dispatch
  )
)(StarArch);