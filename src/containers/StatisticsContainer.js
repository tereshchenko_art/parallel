import React, {PureComponent} from 'react';

export default class StatisticsContainer extends PureComponent {
  render () {

    return <div>
      {this.props.children}
    </div>;
  }
}