import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Slider from 'material-ui/Slider';

import { setNetworkSpeed, setLatency } from '../actions/ExperimentActions';

import './Slider.scss';

class NetworkPicker extends Component {
  handleLatencyChange = (event, latency) => {
    this.props.setLatency(latency);
  };

  handleNetworkSpeedChange = (event, networkSpeed) => {
    this.props.setNetworkSpeed(networkSpeed);
  };

  render() {
    return <div>
      <span>{`Латентность = ${this.props.latency} мкс`}</span>
      <Slider onChange={this.handleLatencyChange} min={0} max={200} defaultValue={10}/>
      <span>{`Пропускная способность сети = ${this.props.networkSpeed} Мб/с`}</span>
      <Slider onChange={this.handleNetworkSpeedChange} min={10} max={1000} defaultValue={10}/>
    </div>
  }
}

const select = state => ({
  latency: state.experiment.get('latency'),
  networkSpeed: state.experiment.get('networkSpeed')
});

export default connect(select, dispatch =>
  bindActionCreators(
    { setNetworkSpeed, setLatency },
    dispatch
  )
)(NetworkPicker);
