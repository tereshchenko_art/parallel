import React, { Component } from 'react';
import { Tabs, Tab } from 'material-ui/Tabs'
import { SelectField } from 'material-ui';
import MenuItem from 'material-ui/MenuItem';
import PlayArrow from 'react-icons/lib/md/play-arrow'
import Stop from 'react-icons/lib/md/stop'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { hashHistory } from 'react-router'
import FlatButton from 'material-ui/FlatButton';

import { setExperimentState, setProblem } from '../actions/ExperimentActions'
import ProcessorSpeedPicker from '../containers/ProcessorSpeedPicker';
import NetworkPicker from '../containers/NetworkPicker';
import TimeSpent from './TimeSpent';
import Progress from './Progress';
import { IDLE, RUNNING } from '../constants/ExperimentStates';
import { CIRCLE, GRID, LINE } from '../constants/Topography';
import { MATRIX_BY_VECTOR, SORTING } from '../constants/Problems';
import { CIRCLE_PROCESSORS, GRID_PROCESSORS, LINEAR_PROCESSORS } from '../constants/ProcessorNumbers';
import { setTypography, resetExperimentState, setProcessorNumber } from '../actions/ExperimentActions';

import './ExperimentContainer.scss';

const SETTINGS = 'SETTINGS';
const EXPERIMENT = 'EXPERIMENT';

class ExperimentContainer extends Component {
  constructor() {
    super();

    this.state = {
      value: SETTINGS,
      archValue: undefined
    }
  }

  handleSetTypographyChange = (event, index, value) => {
    const { setTypography } = this.props;
    const { push } = hashHistory;

    switch (value) {
      case LINE :
        push('/experiment/linear');
        break;
      case GRID:
        push('/experiment/grid');
        break;
      case CIRCLE :
        push('/experiment/circle');
        break;
    }

    setTypography(value);
  };

  handleChange = value => {
    this.setState({
      value
    });
  };

  handleProblemChange = (event, index, value) => {
    this.props.setProblem(value);
  };

  handleProcessorNumberChange = (event, index, value) => {
    this.props.setProcessorNumber(value);
  };

  componentDidMount() {
    this.props.setExperimentState(IDLE);
  }

  handleStartExperimentClick = () => {
    this.props.setExperimentState(RUNNING);
  };

  handleStopExperimentClick = () => {
    this.props.setExperimentState(IDLE);
  };

  resolveProcessors() {
    const { typography } = this.props;

    switch (typography) {
      case LINE:
        return LINEAR_PROCESSORS;

      case GRID:
        return GRID_PROCESSORS;

      case CIRCLE:
        return CIRCLE_PROCESSORS;
    }
  }

  render() {
    const { problem, typography, experimentState, processorsNumber } = this.props;

    return <div>
      <Tabs
        value={this.state.value}
        onChange={this.handleChange}
      >
        <Tab label='Експеримент' value={EXPERIMENT}>
          <div className='experimentContainer'>
            {this.props.children}
            <FlatButton onClick={this.handleStartExperimentClick}
                        icon={<PlayArrow size={40}/>}/>
            <FlatButton onClick={this.handleStopExperimentClick}
                        icon={<Stop size={40}/>}/>
            <TimeSpent/>
            <Progress/>
          </div>
        </Tab>
        <Tab disabled={experimentState === RUNNING} label='Настройки' value={SETTINGS}>
          <div className='experimentContainer'>
            <SelectField floatingLabelText='Топология' value={typography} onChange={this.handleSetTypographyChange}>
              <MenuItem primaryText={LINE} key={LINE} value={LINE}/>
              <MenuItem primaryText={GRID} key={GRID} value={GRID}/>
              <MenuItem primaryText={CIRCLE} key={CIRCLE} value={CIRCLE}/>
            </SelectField>
            <SelectField floatingLabelText='Тип задачи' value={problem} onChange={this.handleProblemChange}>
              <MenuItem primaryText={'Матричные операции'} key={MATRIX_BY_VECTOR} value={MATRIX_BY_VECTOR}/>
              <MenuItem primaryText={'Сортировка'} key={SORTING} value={SORTING}/>
            </SelectField>
            <SelectField floatingLabelText='Количество процессоров' value={processorsNumber}
                         onChange={this.handleProcessorNumberChange}>
              {this.resolveProcessors().map(processorItem => <MenuItem primaryText={processorItem}
                                                                       key={processorItem}
                                                                       value={processorItem}/>)}
            </SelectField>
            <ProcessorSpeedPicker/>
            <NetworkPicker/>
          </div>
        </Tab>
      </Tabs>
    </div>
  }
}

const select = state => {
  const { experiment } = state;

  return {
    typography: experiment.get('typography'),
    problem: experiment.get('problem'),
    experimentProgress: experiment.get('experimentProgress'),
    experimentState: experiment.get('experimentState'),
    processorsNumber: experiment.get('processorsNumber')
  }
};

export default connect(select, dispatch =>
  bindActionCreators(
    {
      setExperimentState,
      setProblem,
      setTypography,
      resetExperimentState,
      setProcessorNumber
    },
    dispatch
  )
)(ExperimentContainer);
