import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Slider from 'material-ui/Slider';

import { setProcessorSpeed } from '../actions/ExperimentActions';

import './Slider.scss';

class SlidePicker extends Component {
  handleSliderChange = (event, processorSpeed) => {
    this.props.setProcessorSpeed(processorSpeed);
  };

  render() {
    return <div>
      <span className='slider-label'>{`Производительность = ${this.props.processorSpeed} GFlops`}</span>
      <Slider onChange={this.handleSliderChange} defaultValue={0.5} />
    </div>
  }
}

const select = state => ({
  processorSpeed: state.experiment.get('processorSpeed')
});

export default connect(select, dispatch =>
  bindActionCreators(
    { setProcessorSpeed },
    dispatch
  )
)(SlidePicker);
