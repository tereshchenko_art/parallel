import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'


import AnimationPair from '../components/AnimationPair';

import { RUNNING, IDLE } from '../constants/ExperimentStates';
import { setExperimentProgress, setTimeSpent, stopExperiment } from '../actions/ExperimentActions';
import { ANIMATION_DURATION } from '../constants/Animation';

import './LinearArch.scss';

class LinearArch extends Component {
  constructor(props) {
    super(props);

    this.pair = [];
    this.callbacs = [];

    this.resolveFunctions();
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.experimentState === IDLE && nextProps.experimentState === RUNNING) {
      this.startFunction();
    }

    if (this.props.experimentState === RUNNING && nextProps.experimentState === IDLE) {
      this.stop();
    }

    if (this.props.processorsNumber !== nextProps.processorsNumber) {
      this.resolveFunctions(nextProps.processorsNumber);
    }
  }

  resolveFunctions(number) {
    const procNumber = number ? number : this.props.processorsNumber;

    switch (procNumber) {
      case 6:
        this.renderFunction = this.generatePairs_6;
        this.startFunction = this.start_6;
        this.animationStepProgress = 100 / 5;
        break;

      case 4:
        this.renderFunction = this.generatePairs_4;
        this.startFunction = this.start_4;
        this.animationStepProgress = 100 / 4;
        break;

      case 2:
        this.renderFunction = this.generatePairs_2;
        this.startFunction = this.start_2;
        this.animationStepProgress = 100;
    }
  }

  stop() {
    this.pair.forEach(pair => {
      if (pair === null) {
        return;
      }
      pair.handleLeftMessageTransitionEnd();
      pair.handleRightMessageTransitionEnd();
    });

    this.callbacs.forEach(timeoutID => {


      clearTimeout(timeoutID);
    });

    this.props.stopExperiment();
  }

  generatePairs_6() {
    const pairs = [0, 1, 2];

    return pairs.map(index => <AnimationPair className={'linear-arch-message'} key={index}
                                             ref={pair => this.pair[index] = pair}/>);
  }

  generatePairs_4() {
    const pairs = [0, 1];

    return pairs.map(index => <AnimationPair className={'linear-arch-message'} key={index}
                                             ref={pair => this.pair[index] = pair}/>);
  }

  generatePairs_2() {
    const pairs = [0];

    return pairs.map(index => <AnimationPair className={'linear-arch-message'} key={index}
                                             ref={pair => this.pair[index] = pair}/>);
  }

  swapAll() {
    this.pair.forEach(pair => {
      pair.animateBoth();
    })
  }

  swapPartially() {
    this.pair[0].animateFromRightToLeft();
    this.pair[1].animateBoth();
    this.pair[2].animateFromLeftToRight();
  }

  start_6() {
    const { setExperimentProgress, setTimeSpent } = this.props;

    this.swapAll();
    this.callbacs.push(setTimeout(() => {
      setExperimentProgress(this.props.experimentProgress + this.animationStepProgress);
      setTimeSpent(20);
      this.swapPartially();
      this.callbacs.push(setTimeout(() => {
        setExperimentProgress(this.props.experimentProgress + this.animationStepProgress);
        setTimeSpent(38);
        this.swapAll();
        this.callbacs.push(setTimeout(() => {
          setExperimentProgress(this.props.experimentProgress + this.animationStepProgress);
          this.swapPartially();
          setTimeSpent(60);
          this.callbacs.push(setTimeout(() => {
            setExperimentProgress(this.props.experimentProgress + this.animationStepProgress);
            this.swapAll();
            setTimeSpent(80);
            setTimeout(() => {
              setTimeSpent(100);
              setExperimentProgress(this.props.experimentProgress + this.animationStepProgress);
            });
          }, ANIMATION_DURATION))
        }, ANIMATION_DURATION))
      }, ANIMATION_DURATION))
    }, ANIMATION_DURATION))
  }

  start_4() {
    const { setExperimentProgress, setTimeSpent } = this.props;
    //todo improve animations
    this.pair[1].animateBoth();
    this.pair[0].animateBoth();
    console.log(this.animationStepProgress);

    this.callbacs.push(setTimeout(() => {
      setExperimentProgress(this.props.experimentProgress + this.animationStepProgress);
      setTimeSpent(20);
      this.pair[0].animateFromRightToLeft();
      this.pair[1].animateBoth();
      this.callbacs.push(setTimeout(() => {
        setExperimentProgress(this.props.experimentProgress + this.animationStepProgress);
        setTimeSpent(38);
        this.pair[1].animateBoth();
        this.pair[0].animateBoth();
        this.callbacs.push(setTimeout(() => {
          setExperimentProgress(this.props.experimentProgress + this.animationStepProgress);
          this.pair[0].animateFromRightToLeft();
          this.pair[1].animateBoth();
          setTimeSpent(60);
          this.callbacs.push(setTimeout(() => {
            setExperimentProgress(this.props.experimentProgress + this.animationStepProgress);
          }, ANIMATION_DURATION))
        }, ANIMATION_DURATION))
      }, ANIMATION_DURATION))
    }, ANIMATION_DURATION))
  }

  start_2() {
    const { setExperimentProgress } = this.props;

    this.pair[0].animateBoth();
    this.callbacs.push(setTimeout(() => {
      setExperimentProgress(this.props.experimentProgress + this.animationStepProgress);
    }, ANIMATION_DURATION))
  }


  render() {
    return <div className={'linear-arch-container'}>
      {this.renderFunction()}
    </div>
  }
}

const select = (state) => {
  const { experiment } = state;

  return {
    experimentState: experiment.get('experimentState'),
    experimentProgress: experiment.get('experimentProgress'),
    processorsNumber: experiment.get('processorsNumber')
  }
};

export default connect(select, dispatch =>
  bindActionCreators(
    { setExperimentProgress, setTimeSpent, stopExperiment },
    dispatch
  )
)(LinearArch);