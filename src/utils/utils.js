export const generateTransition = duration => `left ${duration}s linear, top ${duration}s linear`;

export const animationGenerator = (animationArray, delay) => {
  let localDelay = 0;

  animationArray.forEach((animationCallback) => {
    debugger;
    setTimeout(animationCallback, localDelay);

    localDelay += delay;
  });
};
